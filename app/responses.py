from flask import jsonify


def ok_response(data):
    return jsonify(
        {
            "success": True,
            "data": data
        }
    ), 200


def error_response(error_code: int, description: str):
    return jsonify(
        {
            "success": False,
            "error": {
                "code": error_code,
                "message": description
            }

        }
    ), 200


def requires_params(*params):
    return 400, "Request requires following parameters: {params}".format(params=params)


def invalid_body(description):
    return 400, f"{description}\nRequest body does not follow the specification. Refer to the documentation."


def messenger_does_not_exist(messenger):
    return 400, "Messenger {messenger} does not exist".format(messenger=messenger)


def messenger_already_started(messenger):
    return 412, "Messenger {messenger} already started".format(messenger=messenger)


def messenger_already_stopped(messenger):
    return 412, "Messenger {messenger} already stopped".format(messenger=messenger)
