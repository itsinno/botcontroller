��          �   %   �      p  8   q     �     �     �  M   �  (        ;  N   @  u   �  >    
   D     O     [     g     l  9   s     �  )   �  K   �  �   (  !   �     �  
   �     �     �  &        *  S  B  8   �     �     �     �  M   �  (   7	     `	  N   e	  v   �	  >  +
  
   j     u     �     �     �  9   �     �  )   �  K     �   N  !   �     �  
              $  &   ,     S                                          
                      	                                                           Authorization is done. Send me '/start' to start working Back to menu CANCEL Close For canceling the process and returning to the menu press on button 'CANCEL'. Give the title for your request, please. Help I'm sorry, you could not use anything except text and emoji. Please, try again If you want to report about new problem: send me 'new request'
If you want to see your requests: send me'my requests' It is a list of your requests. In order to see the next page of your requests (if it is possible) click 'Next'. Analogically to see the previous page click 'Prev'. If you want to close, supplement or see in details some of the request press on a correspondent button and choose desired action ('Supplement' or 'Close') Messages:  My requests New request Next Page:  Please, send message that desired to be added to a ticket Prev Process was canceled. You are in the menu Sorry, I can't understand you. Please send me 'Help' or use command '/help' Sorry, I don't understand you.In order to edit ticket send me either 'Close'(to close ticket) or 'Supplement'(to supplement ticket) Sorry, you don't have any request Status:  Supplement The operation completed. Title:  You are in menu. Select desired option Your ticket was closed. Project-Id-Version: 
POT-Creation-Date: 2019-08-16 19:46+0300
PO-Revision-Date: 2019-08-16 20:13+0300
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
X-Generator: Poedit 2.2.3
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: en
 Authorization is done. Send me '/start' to start working Back to menu CANCEL Close For canceling the process and returning to the menu press on button 'CANCEL'. Give the title for your request, please. Help I'm sorry, you could not use anything except text and emoji. Please, try again If you want to report about new problem: send me 'new request'
If you want to see your requests: send me 'my requests' It is a list of your requests. In order to see the next page of your requests (if it is possible) click 'Next'. Analogically to see the previous page click 'Prev'. If you want to close, supplement or see in details some of the request press on a correspondent button and choose desired action ('Supplement' or 'Close') Messages:  My requests New request Next Page:  Please, send message that desired to be added to a ticket Prev Process was canceled. You are in the menu Sorry, I can't understand you. Please send me 'Help' or use command '/help' Sorry, I don't understand you. In order to edit ticket send me either 'Close' (to close ticket) or 'Supplement' (to supplement ticket) Sorry, you don't have any request Status:  Supplement The operation completed. Title:  You are in menu. Select desired option Your ticket was closed. 