# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2019-08-16 19:46+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=cp1251\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: strings.py:2
msgid "You are in menu. Select desired option"
msgstr ""

#: strings.py:3
msgid ""
"If you want to report about new problem: send me 'new request'\n"
"If you want to see your requests: send me'my requests'"
msgstr ""

#: strings.py:5
msgid "Give the title for your request, please."
msgstr ""

#: strings.py:7
msgid "It is a list of your requests. In order to see the next page of your requests (if it is possible) click 'Next'. Analogically to see the previous page click 'Prev'. If you want to close, supplement or see in details some of the request press on a correspondent button and choose desired action ('Supplement' or 'Close')"
msgstr ""

#: strings.py:11
msgid "Sorry, I can't understand you. Please send me 'Help' or use command '/help'"
msgstr ""

#: strings.py:12
msgid "For canceling the process and returning to the menu press on button 'CANCEL'."
msgstr ""

#: strings.py:13
msgid "Process was canceled. You are in the menu"
msgstr ""

#: strings.py:14
msgid "Sorry, you don't have any request"
msgstr ""

#: strings.py:15
msgid "The operation completed."
msgstr ""

#: strings.py:16
msgid "I'm sorry, you could not use anything except text and emoji. Please, try again"
msgstr ""

#: strings.py:27
msgid "Authorization is done. Send me '/start' to start working"
msgstr ""

#: strings.py:28
msgid "Your ticket was closed."
msgstr ""

#: strings.py:29
msgid "Please, send message that desired to be added to a ticket"
msgstr ""

#: strings.py:30
msgid "Sorry, I don't understand you.In order to edit ticket send me either 'Close'(to close ticket) or 'Supplement'(to supplement ticket)"
msgstr ""

#: strings.py:35
msgid ""
"You got new answer on ticket {ticket_number}. \n"
"Title: {title}\n"
"Ticket state {status}\n"
"New replies: "
msgstr ""

#: strings.py:39
msgid ""
"Reply #{article_number}\n"
"From {from_user} at {time}\n"
"Subject: {subject}\n"
"\n"
"{text}"
msgstr ""

#: strings.py:44
msgid "Back to menu"
msgstr ""

#: strings.py:46
msgid "Next"
msgstr ""

#: strings.py:47
msgid "Prev"
msgstr ""

#: strings.py:48
msgid "Help"
msgstr ""

#: strings.py:49
msgid "New request"
msgstr ""

#: strings.py:50
msgid "My requests"
msgstr ""

#: strings.py:51
msgid "CANCEL"
msgstr ""

#: strings.py:52
msgid "Supplement"
msgstr ""

#: strings.py:53
msgid "Close"
msgstr ""

#: strings.py:54
msgid "Page: "
msgstr ""

#: strings.py:55
msgid "Title: "
msgstr ""

#: strings.py:56
msgid "Status: "
msgstr ""

#: strings.py:57
msgid "Messages: "
msgstr ""

