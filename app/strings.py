# -*- coding: UTF-8 -*-
START_MSG = "You are in menu. Select desired option"
HELP_MSG = "If you want to report about new problem: send me 'new request'\nIf you want to see your requests: send me" \
           "'my requests'"
NEW_REQUEST_TITLE_MSG = "Give the title for your request, please." 
NEW_REQUEST_BODY_MSG = "Describe details of your problem, please." 
MY_REQUESTS_MSG = "It is a list of your requests. In order to see the next page of your requests (if it is possible) " \
                  "click 'Next'. Analogically to see the previous page click 'Prev'. If you want to close, " \
                  "supplement or see in details some of the request press on a correspondent button and choose desired " \
                  "action ('Supplement' or 'Close'). If list doesn't arise please wait it will occur soon"
UNKNOWN_MSG = "Sorry, I can't understand you. Please send me 'Help' or use command '/help'"
CANCEL_MSG = "For canceling the process and returning to the menu press on button 'CANCEL'." 
CANCELED_MSG = "Process was canceled. You are in the menu"
NO_REQUESTS_MSG = "Sorry, you don't have any request"
DONE_MSG = "The operation completed." 
ONLY_TEXT_MSG = "I'm sorry, you could not use anything except text and emoji. Please, try again"
AUTH_MSG = "Sorry, you are not signed up. Please, send me your working email to sign up. \n\n" \
           "Извините, вы не зарегистрировались. Пожалуйста, пришлите мне свою рабочую электронную почту, " \
           "чтобы зарегистрироваться." 
INCORRECT_EMAIL_MSG = "You entered incorrect email. Please enter a valid address in " \
                      "innopolis. ru or innopolis. university domain\n\n" \
                      "Вы ввели несуществующий email. Пожалуйста, введите действующий адрес, " \
                      "принадлежащий домену innopolis. ru или innopolis. university"
NOT_UNIQUE_EMAIL_MSG = "A user with this email is already registered. If it was not you, contact the IT department. \n\n" \
                       "Пользователь с таким email уже зарегистрирован. Если это были не вы — обратитесь в IT отдел"
LANG_MSG = "Please, choose your language. \nПожалуйста, выберите свой язык"
DONE_AUTH_MSG = "Authorization is done. Send me '/start' to start working"
CLOSED_MSG = "Your ticket was closed." 
POST_TICKET_MSG = "Please, send message that desired to be added to a ticket"
UNKNOWN_EDIT_MSG = "Sorry, I don't understand you."  \
                   "In order to edit ticket send me either 'Close'(to close ticket) or 'Supplement'(to supplement ticket)"
WRONG_LANGUAGE_MSG = "Sorry, I can't handle this language. Please, select one of the available language. \n\n" \
                     "Извините, я не знаю этот язык. Пожалуйста, выберите один из доступных языков"

NOTIFICATION_TICKET = "You got new answer on ticket {ticket_number}. \n" \
                      "Title: {title}\n" \
                      "Ticket state {status}\n" \
                      "New replies: "
NOTIFICATION_REPLY = "Reply #{article_number}\n" \
                     "From {from_user} at {time}\n" \
                     "Subject: {subject}\n\n" \
                     "{text}"

BACK_TO_MENU = 'Back to menu'
CHANGE_LANGUAGE = 'Change language|Смена языка'
NEXT_PAGE = 'Next'
PREV_PAGE = 'Prev'
HELP = 'Help'
NEW_REQUEST = 'New request'
MY_REQUESTS = 'My requests'
CANCEL = 'CANCEL'
SUPPLEMENT = 'Supplement'
CLOSE = 'Close'
PAGE = 'Page: '
TITLE = 'Title: '
STATUS = 'Status: '
MESSAGE = 'Messages: '
