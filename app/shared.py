from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from os import environ
import functools

messengers = {}

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = '{type}://{username}:{password}@{host}/{database}'.format(
    type=environ['DB_TYPE'],
    username=environ['DB_USERNAME'],
    password=environ['DB_PASSWORD'],
    host=environ['DB_HOST'],
    database=environ['DB_DATABASE']
)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy()

db.init_app(app)

Session = db.session


def db_read(function):
    @functools.wraps(functools)
    def wrapper(*args, **kwargs):
        with app.app_context():
            session = Session
            ret = function(session, *args, **kwargs)
            session.close()
        return ret

    return wrapper


def db_write(function):
    @functools.wraps(function)
    def wrapper(*args, **kwargs):
        with app.app_context():
            session = Session
            ret = function(session, *args, **kwargs)
            session.commit()
            session.close()
        return ret

    return wrapper
