from flask import jsonify, request
from fastjsonschema.exceptions import JsonSchemaException

import sys
import glob
import os.path
import logging
from typing import List, Optional

from shared import app, messengers
from dbController import *
import responses
import utils
import API
from strings import NOTIFICATION_REPLY, NOTIFICATION_TICKET

urllib_logger = logging.getLogger('urllib3')
urllib_logger.addHandler(logging.FileHandler("logs/urllib.log"))

logger = utils.get_logger(os.path.basename(__file__)[:-3])


@app.route('/')
def ping():
    logger.debug("Got request on /ping")
    logger.debug('body:' + str(request.json))
    return jsonify({"pong": True})


api = API.BackAPI()

bot_module_directory = "bots"
# assert bot_module_directory.count('\\') + bot_module_directory.count('/') == 0  # should be only 1 subfolder
bot_module_template = os.path.join(bot_module_directory, "*Bot.py")  # "bots/*.py"
script_path = os.path.realpath(__file__)
script_directory = os.path.dirname(script_path)
os.chdir(script_directory)

senders = {}  # functions used to send message to a particular messenger


@app.route('/notify', methods=["POST"])
def notify():
    logger.log(logging.DEBUG, "new notification:\n" + str(request.json))
    try:
        utils.is_valid(request.json, 'notify')
    except JsonSchemaException as e:
        logger.error("/notify: got an inappropriate body")
        logger.error(str(request.json))
        logger.error(e)
        return responses.error_response(*responses.invalid_body(e.message))
    data = request.json
    counter = data['counter']
    ticket = API.types.Ticket.de_json(data['ticket'])
    user = get_user_by_email(email=ticket.articles[0].from_user.email)
    messenger = user.messenger
    user_id = user.user_id
    _ = utils.languages[user.language]
    any_visible = any([article.is_visible_for_customer for article in ticket.articles[-counter:]])
    if any_visible:
        message = f'''{_(NOTIFICATION_TICKET).format(
            ticket_number=ticket.ticket_number,
            title=ticket.title,
            status=ticket.state
        )}\n\n'''
        for i in range(-counter, 0):
            article = ticket.articles[i]
            if article.is_visible_for_customer:
                message += f"""{_(NOTIFICATION_REPLY).format(
                    article_number=article.article_number,
                    from_user=article.from_user.name,
                    time=article.create_time,
                    subject=article.subject,
                    text=article.body
                )}\n\n"""

        senders[messenger](user_id, message)
    return responses.ok_response({})


def describe_messenger(messenger, accepting_messages: Optional[bool] = None):
    if accepting_messages is None:
        return {
            "name": messenger,
            "acceptingMessages": messengers[messenger]
        }
    else:
        return {
            "name": messenger,
            "acceptingMessages": accepting_messages
        }


# @app.route('/showMessengers')
@app.route('/isAlive', methods=['POST'])
def show_messengers():  # show status of all loaded messengers
    try:
        utils.is_valid(request.json, 'messenger')
    except JsonSchemaException as e:
        logger.error("/isAlive: got an inappropriate body")
        logger.error(str(request.json))
        logger.error(e)
        return responses.error_response(*responses.invalid_body(e.message))

    messenger = request.json.get('messenger')
    if messenger is None:
        return responses.ok_response([describe_messenger(k, v) for k, v in messengers.items()])
    elif messenger in messengers:
        return responses.ok_response(describe_messenger(messenger))
    else:
        return responses.error_response(*responses.messenger_does_not_exist(messenger))


@app.route('/isAlive/count', methods=['POST'])
def messengers_count():
    total = len(messengers)
    accepting_messages = 0
    for k, v in messengers.items():
        if v:
            accepting_messages += 1

    return responses.ok_response({
        'total': total,
        'acceptingMessages': accepting_messages,
        'denyingMessages': total - accepting_messages
    })


@app.route('/start', methods=['POST'])
def messenger_start():  # start requires messenger
    try:
        utils.is_valid(request.json, 'messenger')
    except JsonSchemaException as e:
        logger.error("/start: got an inappropriate body")
        logger.error(str(request.json))
        logger.error(e)
        return responses.error_response(*responses.invalid_body(e.message))

    messenger = request.json.get('messenger')
    if messenger is None:
        return responses.error_response(*responses.requires_params('messenger'))
    elif messenger in messengers:
        accepting_messages = messengers[messenger]
        if not accepting_messages:
            messengers[messenger] = True
            return responses.ok_response(describe_messenger(messenger, True))
        else:
            return responses.error_response(*responses.messenger_already_started(messenger))
    else:
        return responses.error_response(*responses.messenger_does_not_exist(messenger))


@app.route('/stop', methods=['POST'])
def messenger_stop():  # stop required messenger
    try:
        utils.is_valid(request.json, 'messenger')
    except JsonSchemaException as e:
        logger.error("/stop: got an inappropriate body")
        logger.error(str(request.json))
        logger.error(e)
        return responses.error_response(*responses.invalid_body(e.message))

    messenger = request.json.get('messenger')
    if messenger is None:
        return responses.error_response(*responses.requires_params('messenger'))
    elif messenger in messengers:
        accepting_messages = messengers[messenger]
        if accepting_messages:
            messengers[messenger] = False
            return responses.ok_response(describe_messenger(messenger, False))
        else:
            return responses.error_response(*responses.messenger_already_stopped(messenger))
    else:
        return responses.error_response(*responses.messenger_does_not_exist(messenger))


def setup():
    with app.app_context():
        db.create_all(app=app)
    for bot in glob.iglob(bot_module_template):  # find all bot modules
        _, module = bot[:-3].replace('/', '.').split(
            '.')  # 'bots\<module>.py'
        sender, accept_messages = utils.load_module(bot_module_directory, module)
        senders.update({module: sender})
        messengers.update({module: accept_messages})


setup()
logger.info("System is started")
