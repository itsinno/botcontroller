from shared import db_write, db_read
from models import *
from utils import get_logger

import logging

logger = get_logger("dbController")

LANG_BY_EMOJI = {'🇷🇺': 'RU', '🇺🇸': 'EN'}
AVAILABLE_LANGUAGES = ['EN', 'RU']
DEFAULT_LANGUAGE = AVAILABLE_LANGUAGES[0]


@db_write
def create_user(session, email: str, messenger: str, user_id: str, language: str = None) -> None:
    if language is None:
        language = DEFAULT_LANGUAGE
    if language not in AVAILABLE_LANGUAGES:
        raise KeyError(f"Language {language} is not in the list of available languages: {AVAILABLE_LANGUAGES}")
    session.add(User(email=email, language=language, user_id=user_id, messenger=messenger))


@db_read
def get_user_by_email(sesion, email: str) -> User:
    user = User.query.filter_by(email=email).first()
    return user


@db_read
def get_user_by_messenger(session, messenger: str, user_id: str) -> User:
    user = User.query.filter_by(messenger=messenger, user_id=user_id).first()
    if user is None:
        raise KeyError(f"User with messenger {messenger}/{user_id} does not exists")
    return user


@db_write
def change_language_by_email(session, email: str, language: str) -> None:
    if language not in AVAILABLE_LANGUAGES:
        raise KeyError(f"Language {language} is not in the list of available languages: {AVAILABLE_LANGUAGES}")
    User.query.filter_by(email=email).first().language = language


@db_write
def change_language(session, user: User, language: str) -> None:
    change_language_by_email(email=user.email, language=language)
