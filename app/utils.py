import importlib
from os import listdir
from os.path import join
from json import load
from typing import Callable
from logging import getLogger, Formatter, StreamHandler, FileHandler, Logger, INFO, DEBUG, ERROR
import sys

import gettext
import fastjsonschema
from pyisemail import is_email

import dbController

CONSOLE_LOGGING_LEVEL = ERROR
SCHEMAS_FOLDER = 'schemas'
schemas = {}
languages = {
    "RU": gettext.translation('strings', localedir='languages', languages=['ru']),
    "EN": gettext.translation('strings', localedir='languages', languages=['eng'])
}

for key, language in languages.items():
    language.install()
    languages.update({key: language.gettext})

for schema_file in listdir(SCHEMAS_FOLDER):
    with open(join(SCHEMAS_FOLDER, schema_file), 'r') as json:
        schemas[schema_file[:-5]] = fastjsonschema.compile(load(json))


def is_valid(data: dict, schema_name: str) -> bool:
    schemas[schema_name](data)
    return True


def load_module(directory: str, module: str):
    module = importlib.import_module("{0}.{1}".format(directory, module))
    sender = module.send_message
    accept_messages = True
    return sender, accept_messages


def check_email(email: str) -> bool:
    existing_email = is_email(email)
    if existing_email:
        return email.split("@")[1] in ['innopolis.university', 'innopolis.ru']
    else:
        return False


def get_lang_by_user_id(user_id: str, messenger: str) -> Callable[[str], str]:
    lang = dbController.get_user_by_messenger(messenger=messenger, user_id=user_id).language
    return languages[lang]


def get_logger(name: str) -> Logger:
    logger = getLogger(name)
    formatter = Formatter(
        '%(asctime)s (%(filename)s:%(lineno)d %(threadName)s) %(levelname)s - %(name)s: "%(message)s"'
    )
    console_output_handler = StreamHandler(sys.stderr)
    console_output_handler.setFormatter(formatter)
    console_output_handler.setLevel(CONSOLE_LOGGING_LEVEL)

    file_handler = FileHandler(f"logs/{name}.info.log")
    file_handler.setFormatter(formatter)
    file_handler.setLevel(INFO)

    file_error_handler = FileHandler(f"logs/{name}.error.log")
    file_error_handler.setFormatter(formatter)
    file_error_handler.setLevel(ERROR)

    file_debug_handler = FileHandler(f"logs/{name}.debug.log")
    file_debug_handler.setFormatter(formatter)
    file_debug_handler.setLevel(DEBUG)

    logger.addHandler(console_output_handler)
    logger.addHandler(file_handler)
    logger.addHandler(file_error_handler)
    logger.addHandler(file_debug_handler)
    return logger
