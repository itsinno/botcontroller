import telebot
from telebot.types import *
from flask import request, abort

import dbController
import API
from strings import *
from shared import app, messengers

from time import sleep
from os import environ
from os.path import basename
from typing import List
from math import ceil

from utils import get_lang_by_user_id, check_email, languages, get_logger

MESSENGER = basename(__file__)[:-3]
TOKEN = environ['TELEGRAM_TOKEN']
WEBHOOK_BASE = environ['WEBHOOK_BASE_URL']
WEBHOOK_URL_PATH = '/{0}'.format(TOKEN)
BUTTONS_PER_PAGE = 6

bot = telebot.TeleBot(TOKEN)
back = API.BackAPI()
logger = get_logger(MESSENGER)


def send_message(user_id, message, **kwargs) -> None:
    global bot
    bot.send_message(int(user_id), message, **kwargs)


def check_auth(message: Message):
    user = None
    try:
        user = dbController.get_user_by_messenger(messenger=MESSENGER, user_id=str(message.from_user.id)).email
    except KeyError:
        bot.send_message(message.chat.id, AUTH_MSG)
        bot.register_next_step_handler(message, register_user_email)
    return user


def register_user_email(message: Message):
    if check_email(message.text):
        if dbController.get_user_by_email(email=message.text) is None:
            bot.send_message(message.chat.id, LANG_MSG, reply_markup=lang_keyboard())
            bot.register_next_step_handler(message, register_user_lang, message.text)
        else:
            bot.send_message(message.chat.id, NOT_UNIQUE_EMAIL_MSG)
            bot.register_next_step_handler(message, register_user_email)
    else:
        bot.send_message(message.chat.id, INCORRECT_EMAIL_MSG)
        bot.register_next_step_handler(message, register_user_email)


def register_user_lang(message: Message, email: str):
    try:
        chosen_lang = dbController.LANG_BY_EMOJI[message.text]
        dbController.create_user(email=email, messenger=MESSENGER, user_id=str(message.from_user.id),
                                 language=chosen_lang)
        _ = languages[chosen_lang]

        bot.send_message(message.chat.id, _(DONE_MSG),
                         reply_markup=menu_keyboard(message.from_user.id))
    except KeyError:
        logger.error(
            f"User {message.from_user.first_name} {message.from_user.username} {message.from_user.last_name}"
            f" sent incorrect language"
        )
        logger.error(f"Message html_text: {message.html_text}")
        bot.send_message(message.chat.id, WRONG_LANGUAGE_MSG)
        bot.register_next_step_handler(message, register_user_lang, email)


def change_lang(message):
    logger.debug(f"Set_lang: message.text={message.text}, from={message.from_user.username}")
    try:
        lang = dbController.LANG_BY_EMOJI[message.text]
        user = dbController.get_user_by_messenger(messenger=MESSENGER, user_id=str(message.from_user.id))
        dbController.change_language(user=user, language=lang)
        logger.debug(f"Set_lang: lang in try = {lang}, user.email={user.email}, user.lang={user.language}")
        _ = get_lang_by_user_id(str(message.from_user.id), MESSENGER)
        bot.send_message(message.chat.id, _(DONE_MSG),
                         reply_markup=menu_keyboard(message.from_user.id))
    except KeyError:
        logger.error(
            f"User {message.from_user.first_name} {message.from_user.username} {message.from_user.last_name}"
            f" sent incorrect language"
        )
        logger.error(f"Message html_text: {message.html_text}")
        bot.send_message(message.chat.id, WRONG_LANGUAGE_MSG)
        bot.register_next_step_handler(message, change_lang)


def lang_keyboard():
    keyboard = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    emoji_of_lang = {'RU': '🇷🇺', 'EN': '🇺🇸'}
    for lang in dbController.AVAILABLE_LANGUAGES:
        keyboard.add(KeyboardButton(text=emoji_of_lang[lang]))
    return keyboard


def edit_keyboard(user_id):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    keyboard = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    keyboard.add(KeyboardButton(_(SUPPLEMENT)),
                 KeyboardButton(_(CLOSE)))
    keyboard.add(KeyboardButton(_(BACK_TO_MENU)))
    return keyboard


def cancel_keyboard(user_id):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    keyboard = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    keyboard.add(KeyboardButton(text=_(CANCEL)))
    return keyboard


def menu_keyboard(user_id):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    markup_menu = ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    markup_menu.add(KeyboardButton(_(HELP)),
                    KeyboardButton(_(NEW_REQUEST)),
                    KeyboardButton(_(MY_REQUESTS))
                    )
    markup_menu.add(KeyboardButton(CHANGE_LANGUAGE))
    return markup_menu


def request_keyboard(user_id: int, total_pages: int, page: int, tickets_on_page: List[API.types.Ticket]):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    keyboard = InlineKeyboardMarkup(row_width=3)
    back_to_menu = InlineKeyboardButton(text=_(BACK_TO_MENU), callback_data="menu")
    next_page = InlineKeyboardButton(text=_(NEXT_PAGE), callback_data=f"page#{page + 1}_{total_pages}")
    prev_page = InlineKeyboardButton(text=_(PREV_PAGE), callback_data=f"page#{page - 1}_{total_pages}")
    keys = []
    for i, v in enumerate(tickets_on_page):
        keys.append(InlineKeyboardButton(text=str(BUTTONS_PER_PAGE * (page - 1) + i + 1),
                                         callback_data=f'ticket#{v.ticket_id}'))
    keyboard.add(*keys)
    if total_pages == 1:
        keyboard.add(back_to_menu)
    elif page == 1:
        keyboard.add(back_to_menu, next_page)
    elif page == total_pages:
        keyboard.add(prev_page, back_to_menu)
    else:
        keyboard.add(prev_page, back_to_menu, next_page)
    return keyboard


@bot.callback_query_handler(func=lambda call: 'ticket#' in call.data)
def callback_ticket(call):
    bot.clear_step_handler(call.message)
    ticket_id = call.data.split("#")[1]
    user_id = call.from_user.id
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    global back
    ticket = back.ticket_get_by_id(ticket_id)
    body = ""
    for i in range(len(ticket.articles)):
        body += f"{i + 1}) {ticket.articles[i].body}\n"
    status = "Opened"
    if ticket.state == 'closed successful':
        status = "Closed"
    bot.send_message(user_id, _(TITLE) + ticket.title + "\n" + _(MESSAGE) + body + "\n" + _(STATUS) + status,
                     reply_markup=edit_keyboard(user_id))
    bot.register_next_step_handler(call.message, lambda m: edit_ticket(m, ticket_id))


@bot.callback_query_handler(func=lambda call: 'page#' in call.data)  # page#<page>_<total_pages>
def callback_page(call):
    user_id = call.from_user.id
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    message = call.message
    data = call.data.split("#")[1].split("_")
    page = int(data[0])
    total_pages = int(data[1])
    email = dbController.get_user_by_messenger(MESSENGER, str(user_id)).email
    global back
    tickets = back.ticket_get_by_user(user_email=email, page=page, page_size=BUTTONS_PER_PAGE)
    titles = f"{_(PAGE)}{page}/{total_pages}\n"
    for i in range(len(tickets)):
        current_ticket = tickets[i]
        titles += f"{str(i + 1 + BUTTONS_PER_PAGE * (page - 1))}){current_ticket.title}\n"
    bot.edit_message_text(text=titles, chat_id=user_id,
                          message_id=message.message_id, inline_message_id=call.inline_message_id,
                          reply_markup=request_keyboard(user_id, total_pages, page, tickets))


@bot.callback_query_handler(func=lambda call: "menu" in call.data)
def callback_menu(call):
    bot.clear_step_handler(call.message)
    message = call.message
    user_id = call.from_user.id
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    bot.send_message(message.chat.id, _(START_MSG), reply_markup=menu_keyboard(user_id))
    bot.register_next_step_handler(message, processor)


@bot.message_handler(content_types=['text'])
@bot.edited_message_handler(content_types=['text'])
def processor(message):
    user_id = message.from_user.id
    user = check_auth(message)
    if user is None:
        return
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message.text == '/start':
        bot.send_message(message.chat.id, _(START_MSG), reply_markup=menu_keyboard(user_id))
    elif message.text == _(HELP) or message.text == '/help':
        bot.send_message(message.chat.id, _(HELP_MSG), reply_markup=menu_keyboard(user_id))
    elif message.text == _(NEW_REQUEST):
        bot.send_message(message.chat.id, _(NEW_REQUEST_TITLE_MSG) +
                         "\n" + _(CANCEL_MSG),
                         reply_markup=cancel_keyboard(user_id))
        bot.register_next_step_handler(message, new_problem_title)
    elif message.text == _(MY_REQUESTS):
        bot.send_message(user_id, text=_(MY_REQUESTS_MSG))
        global back
        tickets = back.ticket_get_by_user(user_email=user)
        total_pages = ceil(len(tickets) / BUTTONS_PER_PAGE)
        if total_pages == 0:
            bot.send_message(user_id, _(NO_REQUESTS_MSG), reply_markup=menu_keyboard(user_id))
            return
        titles = f"{_(PAGE)}1/{total_pages}\n"
        tickets_on_page = list()
        for i in range(min(len(tickets), BUTTONS_PER_PAGE)):
            current_ticket = tickets[i]
            titles += f"{i + 1}){current_ticket.title}\n"
            tickets_on_page.append(current_ticket)
        bot.send_message(user_id, text=titles,
                         reply_markup=request_keyboard(user_id, total_pages, 1, tickets_on_page))
    elif message.text == _(CHANGE_LANGUAGE):
        bot.send_message(message.chat.id, LANG_MSG, reply_markup=lang_keyboard())
        bot.register_next_step_handler(message, change_lang)
    else:
        bot.send_message(message.chat.id, _(UNKNOWN_MSG), reply_markup=menu_keyboard(user_id))


def new_problem_title(message):
    user_id = message.from_user.id
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message.text == _(CANCEL):
        bot.send_message(message.chat.id, _(CANCELED_MSG),
                         reply_markup=menu_keyboard(user_id))
    elif not message.text:
        bot.send_message(message.chat.id, text=_(ONLY_TEXT_MSG))
        bot.register_next_step_handler(message, new_problem_title)
    else:
        title = message.text
        bot.send_message(message.chat.id, _(NEW_REQUEST_BODY_MSG) +
                         "\n" + _(CANCEL_MSG),
                         reply_markup=cancel_keyboard(user_id))
        bot.register_next_step_handler(message, lambda m: new_problem_body(m, title))


def new_problem_body(message, title):
    user_id = message.from_user.id
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message.text == _(CANCEL):
        bot.send_message(message.chat.id, _(CANCELED_MSG),
                         reply_markup=menu_keyboard(user_id))
    elif not message.text:
        bot.send_message(message.chat.id, text=_(ONLY_TEXT_MSG))
        bot.register_next_step_handler(message, lambda m: new_problem_body(m, title))
    else:
        global back
        back.ticket_create(title=title,
                           user_email=dbController.get_user_by_messenger(MESSENGER, str(user_id)).email,
                           body=message.text)
        bot.send_message(message.chat.id, _(DONE_MSG),
                         reply_markup=menu_keyboard(user_id))


def edit_ticket(message, ticket_id):
    user_id = message.from_user.id
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message.text == _(BACK_TO_MENU):
        bot.send_message(message.chat.id, _(START_MSG),
                         reply_markup=menu_keyboard(user_id))
    elif message.text == _(CLOSE):
        back.ticket_close(ticket_id)
        bot.send_message(message.chat.id, _(CLOSED_MSG), reply_markup=menu_keyboard(user_id))
    elif message.text == _(SUPPLEMENT):
        bot.send_message(message.chat.id, _(POST_TICKET_MSG), reply_markup=cancel_keyboard(user_id))
        bot.register_next_step_handler(message, lambda m: post_ticket(m, ticket_id))
    else:
        bot.send_message(message.chat.id, _(UNKNOWN_EDIT_MSG),
                         reply_markup=edit_keyboard(user_id))
        bot.register_next_step_handler(message, lambda m: edit_ticket(m, ticket_id))


def post_ticket(message, ticket_id):
    global back
    user_id = message.from_user.id
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message.text == _(CANCEL):
        bot.send_message(message.chat.id, _(CANCELED_MSG),
                         reply_markup=menu_keyboard(user_id))
    elif not message.text:
        bot.send_message(message.chat.id, text=_(ONLY_TEXT_MSG))
        bot.register_next_step_handler(message, post_ticket)
    else:
        from_user = dbController.get_user_by_messenger(messenger=MESSENGER, user_id=str(user_id)).email
        back.ticket_send_message(ticket_id, message.text, from_user)
        bot.send_message(message.chat.id, _(DONE_MSG), reply_markup=menu_keyboard(user_id))


@app.route(WEBHOOK_URL_PATH, methods=["POST"])
def webhook():
    if request.headers.get('content-type') == 'application/json':
        if messengers[MESSENGER]:
            json_string = request.get_data().decode('utf-8')
            update = telebot.types.Update.de_json(json_string)
            bot.process_new_updates([update])
        return ''
    else:
        abort(403)


@app.route('/setTelegramWebhook', methods=['POST'])
def set_webhook():
    bot.remove_webhook()
    sleep(0.1)
    bot.set_webhook(WEBHOOK_BASE + WEBHOOK_URL_PATH)
    return 'ok'
