from math import ceil
from random import randint

from flask import request
from vk_api.keyboard import VkKeyboard, VkKeyboardColor, VkKeyboardButton
from vk_api import vk_api

from os import environ

import API
import dbController
from strings import *
from shared import app, messengers
from utils import get_lang_by_user_id, check_email

MESSENGER = "vkBot"
TOKEN = environ['VK_TOKEN']
# CONFIRMATION_CODE = environ['VK_CONFIRMATION']
CONFIRMATION_CODE = '3571036f'
SECRET_KEY = environ['VK_SECRET']
BUTTONS_PER_PAGE = 4

bot = vk_api.VkApi(token=TOKEN)

cur_process = dict()
tmp_args = dict()
back = API.BackAPI()


def send_message(user_id, message, keyboard=None):
    bot.method('messages.send', {'user_id': int(user_id), 'message': message,
                                 'keyboard': keyboard, 'random_id': randint(-2147483648, 2147483648)})


def edit_message_text(peer_id, message, message_id, keyboard=None):
    bot.method('messages.edit', {'peer_id': peer_id, 'message': message, 'message_id': message_id,
                                 'keyboard': keyboard})


def register_next_step_handler(user_id, next_step, args=None):
    global cur_process, tmp_args
    cur_process[user_id] = next_step
    tmp_args[user_id] = args


def check_auth(message):
    user = None
    try:
        user = dbController.get_user_by_messenger(messenger=MESSENGER, user_id=str(message['user_id'])).email
        register_next_step_handler(message['user_id'], 'processor')
    except KeyError:
        send_message(message['user_id'], AUTH_MSG)
        register_next_step_handler(message['user_id'], 'auth')
    return user


def auth(message):
    user_id = message['user_id']
    if check_email(message['body']):
        try:
            dbController.create_user(email=message['body'], messenger=MESSENGER, user_id=str(user_id))
            send_message(user_id, LANG_MSG, keyboard=lang_keyboard())
            register_next_step_handler(user_id, 'change_lang')
        except:
            send_message(user_id, NOT_UNIQUE_EMAIL_MSG)
            register_next_step_handler(user_id, 'processor')
    else:
        send_message(user_id, INCORRECT_EMAIL_MSG)
        register_next_step_handler(user_id, 'auth')


def change_lang(message):
    lang_by_emoji = {'🇷🇺': 'RU', '🇺🇸': 'EN'}
    user_id = message['user_id']
    try:
        lang = lang_by_emoji[message['body']]
        user = dbController.get_user_by_messenger(messenger=MESSENGER, user_id=str(user_id))
        dbController.change_language(user=user, language=lang)
        _ = get_lang_by_user_id(str(user_id), MESSENGER)
        send_message(user_id, _(DONE_MSG), keyboard=menu_keyboard(user_id))
        register_next_step_handler(user_id, 'processor')
    except KeyError:
        send_message(user_id, WRONG_LANGUAGE_MSG)
        register_next_step_handler(user_id, WRONG_LANGUAGE_MSG)


def lang_keyboard():
    emoji_of_lang = {'RU': '🇷🇺', 'EN': '🇺🇸'}
    keyboard = VkKeyboard()
    for lang in dbController.AVAILABLE_LANGUAGES:
        keyboard.add_button(emoji_of_lang[lang], color=VkKeyboardColor.DEFAULT)
    return keyboard.get_keyboard()


def menu_keyboard(user_id):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button(_(HELP), color=VkKeyboardColor.PRIMARY)
    keyboard.add_button(_(NEW_REQUEST), color=VkKeyboardColor.PRIMARY)
    keyboard.add_button(_(MY_REQUESTS), color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button(_(CHANGE_LANGUAGE), color=VkKeyboardColor.DEFAULT)
    return keyboard.get_keyboard()


def cancel_keyboard(user_id):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button(_(CANCEL), color=VkKeyboardColor.NEGATIVE)
    return keyboard.get_keyboard()


def request_keyboard(user_id, total_pages, page, tickets_on_page):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    keyboard = VkKeyboard(one_time=True)
    back_to_menu = _(BACK_TO_MENU)
    next_page = _(NEXT_PAGE) + ":" + str(page + 1) + "/" + str(total_pages)
    prev_page = _(PREV_PAGE) + ":" + str(page - 1) + "/" + str(total_pages)
    for i, v in enumerate(tickets_on_page):
        keyboard.add_button(str(i + 1 + BUTTONS_PER_PAGE * (page - 1)),
                            color=VkKeyboardColor.DEFAULT)
    keyboard.add_line()
    if total_pages == 1:
        keyboard.add_button(back_to_menu, color=VkKeyboardColor.PRIMARY)
    elif page == 1:
        keyboard.add_button(back_to_menu, color=VkKeyboardColor.PRIMARY)
        keyboard.add_button(next_page, color=VkKeyboardColor.PRIMARY)
    elif page == total_pages:
        keyboard.add_button(prev_page, color=VkKeyboardColor.PRIMARY)
        keyboard.add_button(back_to_menu, color=VkKeyboardColor.PRIMARY)
    else:
        keyboard.add_button(prev_page, color=VkKeyboardColor.PRIMARY)
        keyboard.add_button(back_to_menu, color=VkKeyboardColor.PRIMARY)
        keyboard.add_button(next_page, color=VkKeyboardColor.PRIMARY)
    return keyboard.get_keyboard()


def edit_keyboard(user_id):
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button(_(CLOSE), color=VkKeyboardColor.NEGATIVE)
    keyboard.add_button(_(SUPPLEMENT), color=VkKeyboardColor.PRIMARY)
    keyboard.add_line()
    keyboard.add_button(_(BACK_TO_MENU), color=VkKeyboardColor.DEFAULT)
    return keyboard.get_keyboard()


def callback_ticket(call):
    ticket_index = int(call['body'])
    user_id = call['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    global back
    email = dbController.get_user_by_messenger(MESSENGER, str(user_id)).email
    ticket = back.ticket_get_by_user(user_email=email, page=ticket_index, page_size=1)[0]
    body = ""
    for i in range(len(ticket.articles)):
        body += f"{i + 1}) {ticket.articles[i].body}\n"
    status = "Opened"
    if ticket.state == 'closed successful':
        status = "Closed"
    send_message(user_id, _(TITLE) + ticket.title + "\n" + _(MESSAGE) + body + "\n" + _(STATUS) + status,
                 keyboard=edit_keyboard(user_id))
    register_next_step_handler(user_id, 'edit_ticket', [ticket.ticket_id])


def callback_page(call):
    global back
    user_id = call['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    email = dbController.get_user_by_messenger(MESSENGER, str(user_id)).email
    data = call['body'].split(":")[1].split("/")
    page = int(data[0])
    total_pages = int(data[1])
    tickets = back.ticket_get_by_user(user_email=email, page=page, page_size=BUTTONS_PER_PAGE)
    titles = _(PAGE) + str(page) + "/" + str(
        total_pages) + "\n"
    tickets_on_page = list()
    for i in range(len(tickets)):
        current_ticket = tickets[i]
        titles += str(i + 1 + BUTTONS_PER_PAGE * (page - 1)) + ")" + str(current_ticket.title) + "\n"
        tickets_on_page.append(current_ticket)
    send_message(user_id, titles, request_keyboard(user_id, total_pages, page, tickets_on_page))
    register_next_step_handler(user_id, 'callback_emulator')


def callback_menu(call):
    user_id = call['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    send_message(user_id, _(START_MSG), menu_keyboard(user_id))
    register_next_step_handler(user_id, 'processor')


def callback_emulator(pseudo_call):
    user_id = pseudo_call['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if pseudo_call['body'] == _(BACK_TO_MENU):
        callback_menu(pseudo_call)
    elif _(NEXT_PAGE) in pseudo_call['body'] or _(PREV_PAGE) in pseudo_call['body']:
        callback_page(pseudo_call)
    else:
        callback_ticket(pseudo_call)


def edit_ticket(message, ticket_id):
    user_id = message['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message['body'] == _(BACK_TO_MENU):
        send_message(user_id, _(START_MSG),
                     keyboard=menu_keyboard(user_id))
        register_next_step_handler(user_id, 'processor')
    elif message['body'] == _(CLOSE):
        back.ticket_close(ticket_id)
        send_message(user_id, _(CLOSED_MSG),
                     keyboard=menu_keyboard(user_id))
        register_next_step_handler(user_id, 'processor')
    elif message['body'] == _(SUPPLEMENT):
        send_message(user_id, _(POST_TICKET_MSG))
        register_next_step_handler(user_id, 'post_ticket', [ticket_id])
    else:
        send_message(user_id, _(UNKNOWN_EDIT_MSG),
                     keyboard=edit_keyboard(user_id))


def post_ticket(message, ticket_id):
    global back
    user_id = message['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message['body'] == _(CANCEL):
        send_message(user_id, _(CANCELED_MSG),
                     keyboard=menu_keyboard(user_id))
    elif not message['body']:
        send_message(user_id, _(ONLY_TEXT_MSG))
    else:
        from_user = dbController.get_user_by_messenger(messenger=MESSENGER, user_id=str(user_id)).email
        back.ticket_send_message(ticket_id, message['body'], from_user)
        send_message(user_id, _(DONE_MSG), keyboard=menu_keyboard(user_id))
        register_next_step_handler(user_id, 'processor')


def processor(message):
    user_id = message['user_id']
    user = check_auth(message)
    if user is None:
        return
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message['body'] == '/start':
        send_message(user_id, _(START_MSG), menu_keyboard(user_id))
    elif message['body'] == _(HELP_MSG) or message['body'] == '/help':
        send_message(user_id, _(HELP_MSG), menu_keyboard(user_id))
    elif message['body'] == _(NEW_REQUEST):
        send_message(user_id, _(NEW_REQUEST_TITLE_MSG) +
                     "\n" + _(CANCEL_MSG), cancel_keyboard(user_id))
        register_next_step_handler(user_id, 'new_problem_title')
    elif message['body'] == _(MY_REQUESTS):
        send_message(user_id, _(MY_REQUESTS_MSG))
        global back
        tickets = back.ticket_get_by_user(user_email=user)
        total_pages = ceil(len(tickets) / BUTTONS_PER_PAGE)
        if total_pages == 0:
            send_message(user_id, _(NO_REQUESTS_MSG), keyboard=menu_keyboard(user_id))
            register_next_step_handler(user_id, 'processor')
            return
        titles = _(PAGE) + str(1) + "/" + str(
            total_pages) + "\n"
        tickets_on_page = list()
        for i in range(min(len(tickets), BUTTONS_PER_PAGE)):
            current_ticket = tickets[i]
            titles += str(i + 1) + ")" + str(current_ticket.title) + "\n"
            tickets_on_page.append(current_ticket)
        send_message(user_id, titles,
                     request_keyboard(user_id, total_pages, 1, tickets_on_page))
        register_next_step_handler(user_id, 'callback_emulator')
    elif message['body'] == _(CHANGE_LANGUAGE):
        send_message(user_id, LANG_MSG, lang_keyboard())
        register_next_step_handler(user_id, 'change_lang')
    else:
        send_message(user_id, _(UNKNOWN_MSG), menu_keyboard(user_id))


def new_problem_title(message):
    user_id = message['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message['body'] == _(CANCEL):
        send_message(user_id, _(CANCELED_MSG),
                     menu_keyboard(user_id))
        register_next_step_handler(user_id, 'processor')
    elif not message['body']:
        send_message(user_id, _(ONLY_TEXT_MSG))
    else:
        send_message(user_id, _(NEW_REQUEST_BODY_MSG) +
                     "\n" + _(CANCEL_MSG), cancel_keyboard(user_id))
        register_next_step_handler(user_id, 'new_problem_body', [message['body']])


def new_problem_body(message, title: str):
    user_id = message['user_id']
    _ = get_lang_by_user_id(str(user_id), MESSENGER)
    if message['body'] == _(CANCEL):
        send_message(user_id, _(CANCELED_MSG),
                     menu_keyboard(user_id))
        register_next_step_handler(user_id, 'processor')
    elif not message['body']:
        send_message(user_id, _(ONLY_TEXT_MSG))
    else:
        global back
        back.ticket_create(title=title,
                           user_email=dbController.get_user_by_messenger(MESSENGER, str(user_id)).email,
                           body=message['body'])
        send_message(user_id, _(DONE_MSG),
                     menu_keyboard(user_id))
        register_next_step_handler(user_id, 'processor')


functions = {'processor': processor, 'new_problem_title': new_problem_title,
             'new_problem_body': new_problem_body, 'callback_emulator': callback_emulator,
             'auth': auth, 'change_lang': change_lang, 'edit_ticket': edit_ticket, 'post_ticket': post_ticket}


def start_process(message):
    global cur_process, tmp_args
    if message['user_id'] not in cur_process:
        cur_process[message['user_id']] = 'processor'
    if message['user_id'] not in tmp_args:
        tmp_args[message['user_id']] = None

    if tmp_args[message['user_id']] is None:
        functions[cur_process[message['user_id']]](message)
    else:
        functions[cur_process[message['user_id']]](message, *tmp_args[message['user_id']])


@app.route('/' + SECRET_KEY, methods=['POST'])
def server_handler():
    data = request.get_json(force=True, silent=True)
    print("Got request from vk with payload " + str(data))
    if not data or 'type' not in data:
        return 'not ok'
    if data['type'] == 'confirmation':
        return CONFIRMATION_CODE
    elif data['type'] == 'message_new':
        if messengers[MESSENGER]:
            event = data['object']
            start_process(event)
        return 'ok'
    return 'ok'
