import os.path
import sys

# file = input('Input a filename to wrap strings\n')
file = 'strings.py'

if not os.path.exists(file):
    print(f"File {file} does not exist")
    sys.exit(0)

multiline = False

with open(file, 'r') as f:
    with open(f"{file.split('.')[0]}_wrapped.py", 'w') as w:
        w.write('# -*- coding: UTF-8 -*-\n')
        for line in f.readlines():
            if not any([line.strip().startswith('#'), len(line.strip()) == 0]):
                line = line.rstrip()
                multiline = line.endswith('\\')
                if line.count('=') == 1:
                    name, line = line.split('=')

                    w.write(f"{name}=_({line}")
                    if multiline:
                        w.write("\n")
                    else:
                        w.write(")\n")
                else:
                    w.write(f"{line}")
                    if not multiline:
                        w.write(")")
                    w.write("\n")
