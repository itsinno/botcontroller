# -*- coding: utf-8 -*-

try:
    import ujson as json
except ImportError:
    import json

import six
from enum import Enum
from email.utils import parseaddr
from typing import List


class TicketState(Enum):
    # https://doc.otrs.com/doc/manual/admin/stable/en/content/ticket-settings/states.html
    CLOSED_SUCCESSFUL = 'closed successful'
    CLOSED_UNSUCCESSFUL = 'closed unsuccessful'
    MERGED = 'merged'
    NEW = 'new'
    OPEN = 'open'
    REMOVED = 'removed'


class JsonSerializable(object):
    """
    Subclasses of this class are guaranteed to be able to be converted to JSON format.
    All subclasses of this class must override to_json.
    """

    def to_json(self):
        """
        Returns a JSON string representation of this class.

        This function must be overridden by subclasses.
        :return: a JSON formatted string.
        """
        raise NotImplementedError


class Dictionaryable(object):
    """
    Subclasses of this class are guaranteed to be able to be converted to dictionary.
    All subclasses of this class must override to_dic.
    """

    def to_dic(self):
        """
        Returns a JSON string representation of this class.

        This function must be overridden by subclasses.
        :return: a JSON formatted string.
        """
        raise NotImplementedError


class JsonDeserializable(object):
    """
    Subclasses of this class are guaranteed to be able to be created from a json-style dict or json formatted string.
    All subclasses of this class must override de_json.
    """

    @classmethod
    def de_json(cls, json_type):
        """
        Returns an instance of this class from the given json dict or string.

        This function must be overridden by subclasses.
        :return: an instance of this class created from the given json dict or string.
        """
        raise NotImplementedError

    @staticmethod
    def check_json(json_type):
        """
        Checks whether json_type is a dict or a string. If it is already a dict, it is returned as-is.
        If it is not, it is converted to a dict by means of json.loads(json_type)
        :param json_type:
        :return:
        """
        str_types = (str,)

        if type(json_type) == dict:
            return json_type
        elif type(json_type) in str_types:
            return json.loads(json_type)
        else:
            raise ValueError("json_type should be a json dict or string.")

    def __str__(self):
        d = {}
        for x, y in six.iteritems(self.__dict__):
            if hasattr(y, '__dict__'):
                d[x] = y.__dict__
            else:
                d[x] = y

        return six.text_type(d)


class UserEmail:
    def __init__(self, email_string: str):
        self.raw = email_string
        self.name, self.email = parseaddr(email_string)
        if not self.name:
            self.name = None
        if not self.email:
            self.email = None


class Article(JsonDeserializable):
    """
    Class standing for messages in requests
    """

    @classmethod
    def de_json(cls, json_string):
        obj = cls.check_json(json_string)
        fields = {}
        article_id = obj.get("ArticleID")
        article_number = obj.get("ArticleNumber")
        body = obj.get("Body")
        from_user = UserEmail(obj.get("From"))
        is_visible_for_customer = obj.get("IsVisibleForCustomer")

        if "Bcc" in obj:
            fields["bcc"] = UserEmail(obj.get("Bcc"))
        if "Cc" in obj:
            fields["cc"] = UserEmail(obj.get("Cc"))
        if "ChangeBy" in obj:
            fields["change_by"] = obj.get("ChangeBy")
        if "ChangeTime" in obj:
            fields["change_time"] = obj.get("ChangeTime")
        if "Charset" in obj:
            fields["charset"] = obj.get("Charset")
        if "CommunicationChannelID" in obj:
            fields["communication_channel_id"] = obj.get("CommunicationChannelID")
        if "ContentCharset" in obj:
            fields["content_charset"] = obj.get("ContentCharset")
        if "ContentType" in obj:
            fields["content_type"] = obj.get("ContentType")
        if "CreateBy" in obj:
            fields["create_by"] = obj.get("CreateBy")
        if "CreateTime" in obj:
            fields["create_time"] = obj.get("CreateTime")
        if "InReplyTo" in obj:
            fields["in_reply_to"] = obj.get("InReplyTo")
        if "IncomingTime" in obj:
            fields["incoming_time"] = obj.get("IncomingTime")
        if "MessageID" in obj:
            fields["message_id"] = obj.get("MessageID")
        if "MimeType" in obj:
            fields["mime_type"] = obj.get("MimeType")
        if "References" in obj:
            fields["references"] = obj.get("References")
        if "ReplyTo" in obj:
            fields["reply_to"] = obj.get("ReplyTo")
        if "SenderType" in obj:
            fields["sender_type"] = obj.get("SenderType")
        if "SenderTypeID" in obj:
            fields["sender_type_id"] = obj.get("SenderTypeID")
        if "Subject" in obj:
            fields["subject"] = obj.get("Subject")
        if "TicketID" in obj:
            fields["ticket_id"] = obj.get("TicketID")
        if "TimeUnit" in obj:
            fields["time_unit"] = obj.get("TimeUnit")
        if "To" in obj:
            fields["to"] = UserEmail(obj.get("To"))

        return cls(article_id, article_number, from_user, body, is_visible_for_customer, fields, json_string)

    def __init__(self, article_id, article_number, from_user:UserEmail, body, is_visible_for_customer, fields, json_string):
        self.article_id = article_id
        self.article_number = article_number
        self.from_user = from_user
        self.body = body
        self.is_visible_for_customer = bool(int(is_visible_for_customer))
        self.bcc = None
        self.cc = None
        self.change_by = None
        self.change_time = None
        self.charset = None
        self.communication_channel_id = None
        self.content_charset = None
        self.content_type = None
        self.create_by = None
        self.create_time = None
        self.in_reply_to = None
        self.incoming_time = None
        self.message_id = None
        self.mime_type = None
        self.references = None
        self.reply_to = None
        self.sender_type = None
        self.sender_type_id = None
        self.subject = None
        self.ticket_id = None
        self.time_unit = None
        self.to = None

        for key in fields:
            setattr(self, key, fields[key])
        self.json_string = json_string


class Ticket(JsonDeserializable):
    @classmethod
    def de_json(cls, json_string):
        obj = cls.check_json(json_string)
        fields = {}
        ticket_number = obj.get("TicketNumber")
        title = obj.get("Title")
        ticket_id = obj.get("TicketID")
        state = obj.get("State")
        articles = list(map(Article.de_json, obj.get('Articles')))

        if "StateID" in obj:
            fields["state_id"] = obj.get("StateID")
        if "StateType" in obj:
            fields["state_type"] = obj.get("StateType")
        if "Priority" in obj:
            fields["priority"] = obj.get("Priority")
        if "PriorityID" in obj:
            fields["priority_id"] = obj.get("PriorityID")
        if "Lock" in obj:
            fields["lock"] = obj.get("Lock")
        if "LockID" in obj:
            fields["lock_id"] = obj.get("LockID")
        if "Queue" in obj:
            fields["queue"] = obj.get("Queue")
        if "QueueID" in obj:
            fields["queue_id"] = obj.get("QueueID")
        if "CustomerID" in obj:
            fields["customer_id"] = obj.get("CustomerID")
        if "CustomerUserID" in obj:
            fields["customer_user_id"] = obj.get("CustomerUserID")
        if "Owner" in obj:
            fields["owner"] = UserEmail(obj.get("Owner"))
        if "OwnerID" in obj:
            fields["owner_id"] = obj.get("OwnerID")
        if "Type" in obj:
            fields["type"] = obj.get("Type")
        if "TypeID" in obj:
            fields["type_id"] = obj.get("TypeID")
        if "SLA" in obj:
            fields["SLA"] = obj.get("SLA")
        if "SLAID" in obj:
            fields["SLA_id"] = obj.get("SLAID")
        if "Service" in obj:
            fields["service"] = obj.get("Service")
        if "ServiceID" in obj:
            fields["service_id"] = obj.get("ServiceID")
        if "Responsible" in obj:
            fields["responsible"] = UserEmail(obj.get("Responsible"))
        if "ResponsibleID" in obj:
            fields["responsible_id"] = obj.get("ResponsibleID")
        if "Age" in obj:
            fields["age"] = obj.get("Age")
        if "Created" in obj:
            fields["created"] = obj.get("Created")
        if "CreateBy" in obj:
            fields["create_by"] = obj.get("CreateBy")
        if "Changed" in obj:
            fields["changed"] = obj.get("Changed")
        if "ChangeBy" in obj:
            fields["change_by"] = obj.get("ChangeBy")
        if "ArchiveFlag" in obj:
            fields["archive_flag"] = obj.get("ArchiveFlag")

        # more fields
        if "EscalationResponseTime" in obj:
            fields["escalation_response_time"] = obj.get("EscalationResponseTime")
        if "EscalationSolutionTime" in obj:
            fields["escalation_solution_time"] = obj.get("EscalationSolutionTime")
        if "EscalationTime" in obj:
            fields["escalation_time"] = obj.get("EscalationTime")
        if "EscalationUpdateTime" in obj:
            fields["escalation_update_time"] = obj.get("EscalationUpdateTime")
        if "GroupID" in obj:
            fields["group_id"] = obj.get("GroupID")
        if "RealTillTimeNotUsed" in obj:
            fields["real_till_time_not_used"] = obj.get("RealTillTimeNotUsed")
        if "TimeUnit" in obj:
            fields["time_unit"] = obj.get("TimeUnit")
        if "UnlockTimeout" in obj:
            fields["unlock_timeout"] = obj.get("UnlockTimeout")
        if "UntilTime" in obj:
            fields["until_time"] = obj.get("UntilTime")

        return cls(ticket_number, title, ticket_id, state, fields, articles, json_string)

    def __init__(self, ticket_number, title, ticket_id, state, fields, articles: List[Article], json_string):
        self.ticket_number = ticket_number
        self.title = str(title)
        self.ticket_id = ticket_id
        self.state = state
        self.articles = articles
        self.state_id = None
        self.state_type = None
        self.priority = None
        self.priority_id = None
        self.lock = None
        self.lock_id = None
        self.queue = None
        self.queue_id = None
        self.customer_id = None
        self.customer_user_id = None
        self.owner = None
        self.owner_id = None
        self.type = None
        self.type_id = None
        self.SLA = None
        self.SLA_id = None
        self.service = None
        self.service_id = None
        self.responsible = None
        self.responsible_id = None
        self.age = None
        self.created = None
        self.create_by = None
        self.changed = None
        self.change_by = None
        self.archive_flag = None

        self.escalation_response_time = None
        self.escalation_solution_time = None
        self.escalation_time = None
        self.escalation_update_time = None
        self.group_id = None
        self.real_till_time_not_used = None
        self.time_unit = None
        self.unlock_timeout = None
        self.until_time = None

        for key in fields:
            setattr(self, key, fields[key])
        self.json = json_string
