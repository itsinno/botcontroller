# -*- coding: utf-8 -*-
from os import environ
from email.utils import parseaddr

try:
    import ujson as json
except ImportError:
    import json

import requests

try:
    from requests.packages.urllib3 import fields

    format_header_param = fields.format_header_param
except ImportError:
    format_header_param = None

from API import util, types, logger

proxy = None

API_URL = environ['BACKEND_BASE_URL'] + "/{0}"
# API_URL = "http://localhost:5000/bot{0}/{1}"
# FILE_URL = "https://api.telegram.org/file/bot{0}/{1}"

DEFAULT_FIELD_LIST = ["TicketNumber", "Title", "TicketID", "State", "Articles"]
CONNECT_TIMEOUT = 3.5
READ_TIMEOUT = 9999


def _get_req_session(reset=False):
    return util.per_thread('req_session', lambda: requests.session(), reset)


def _make_request(  # token,
        method_url, method='post', params=None, files=None, base_url=API_URL):
    """
    Makes a request to the Telegram API.
    :param token: The bot's API token. (Created with @BotFather)
    :param method_url: Name of the API method to be called. (E.g. 'getUpdates')
    :param method: HTTP method to be used. Defaults to 'get'.
    :param params: Optional parameters. Should be a dictionary with key-value pairs.
    :param files: Optional files.
    :return: The result parsed to a JSON dictionary.
    """
    request_url = base_url.format(method_url)
    logger.debug("Request: method={0} url={1} params={2} files={3}".format(method, request_url, params, files))
    read_timeout = READ_TIMEOUT
    connect_timeout = CONNECT_TIMEOUT
    if params:
        if 'timeout' in params: read_timeout = params['timeout'] + 10
        if 'connect-timeout' in params: connect_timeout = params['connect-timeout'] + 10
    result = _get_req_session().request(method, request_url, json=params, files=files,
                                        timeout=(connect_timeout, read_timeout), proxies=proxy)
    logger.debug("The server returned: '{0}'".format(result.text.encode('utf8')))
    return _check_result(method_url, result)


def _check_result(method_name, result):
    """
    Checks whether `result` is a valid API response.
    A result is considered invalid if:
        - The server returned an HTTP response code other than 200
        - The content of the result is invalid JSON.
        - The method call was unsuccessful (The JSON 'success' field equals False)

    :raises ApiException: if one of the above listed cases is applicable
    :param method_name: The name of the method called
    :param result: The returned result of the method request
    :return: The result parsed to a JSON dictionary.
    """
    if result.status_code != 200:
        msg = 'The server returned HTTP {0} {1}. Response body:\n[{2}]' \
            .format(result.status_code, result.reason, result.text.encode('utf8'))
        raise ApiException(msg, method_name, result)

    try:
        result_json = result.json()
    except:
        msg = 'The server returned an invalid JSON response. Response body:\n[{0}]' \
            .format(result.text.encode('utf8'))
        raise ApiException(msg, method_name, result)

    if not result_json['success']:
        msg = 'Error code: {0} Description: {1}' \
            .format(result_json['error']['code'], result_json['error']['message'])
        raise ApiException(msg, method_name, result)
    return result_json['data']


def get_username(email: str) -> str:
    return parseaddr(email)[1].split('@')[0]


def from_pep(s: str):
    data = s.split("_")
    result = ""
    for key in data:
        if key == "id":
            result += 'ID'
        elif key.isupper():
            result += key
        else:
            result += key.capitalize()
    return result


# def to_pep(s):
#     return "_".join(map(str.lower, re.sub(r"([A-Z])", r" \1", s).split()))


def ticket_set_state(ticket_id: int, state: types.TicketState):
    url = "tickets/setState"
    payload = {"state": state.value, "ticketId": ticket_id}
    return _make_request(url, params=payload)


def ticket_close(ticket_id: int):
    return ticket_set_state(ticket_id, types.TicketState.CLOSED_SUCCESSFUL)


def ticket_create(title, user_email, body):
    url = 'createTicket'
    payload = {
        'title': title,
        'user': user_email,
        'article': {
            'subject': "problem",
            'body': body
        }
    }
    return _make_request(url, params=payload)


def ticket_get_by_id(ticket_id: int, additional_fields=None):
    fields = DEFAULT_FIELD_LIST.copy()
    if additional_fields is not None:
        if isinstance(additional_fields, list):
            fields.extend(map(from_pep, additional_fields))
        else:
            raise TypeError("fields must be of type list")
    url = 'tickets/getTicket'
    payload = {
        'ticketId': ticket_id,
        'fields': fields
    }
    return _make_request(url, params=payload)


def ticket_get_by_user(user_email: str, page=None, page_size: int = 5,
                       opened: bool = None, additional_fields: list = None):

    fields = DEFAULT_FIELD_LIST.copy()
    if additional_fields is not None:
        if isinstance(additional_fields, list):
            fields.extend(map(from_pep, additional_fields))
        else:
            raise TypeError("fields must be of type list")
    url = 'tickets/search'
    payload = {
        "fields": fields,
        "opened": opened,
        "CustomerUserLogin": get_username(user_email),
        "pageSize": page_size
    }
    if page is not None:
        payload.update({"page": page})
    return _make_request(url, params=payload)


def ticket_send_message(ticket_id: int, message: str, from_user: str):
    """

    :param ticket_id:
    :param message:
    :param from_user: email of user which sent a message
    :return:
    """
    url = "tickets/addMessage"
    payload = {
        "ticketId": ticket_id,
        "from_user": from_user,
        "message": {
            "subject": "reply",
            "text": message
        }
    }
    return _make_request(url, params=payload)


class ApiException(Exception):
    """
    This class represents an Exception thrown when a call to the Telegram API fails.
    In addition to an informative message, it has a `function_name` and a `result` attribute, which respectively
    contain the name of the failed function and the returned result that made the function to be considered  as
    failed.
    """

    def __init__(self, msg, function_name, result):
        super(ApiException, self).__init__("A request to the OTRS API was unsuccessful. {0}".format(msg))
        self.function_name = function_name
        self.result = result
