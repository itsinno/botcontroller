# -*- coding: utf-8 -*-
from __future__ import print_function

import API.types

import logging
import sys
from typing import Optional, List

# import threading
# import time


logger = logging.getLogger('OTRS')
formatter = logging.Formatter(
    '%(asctime)s (%(filename)s:%(lineno)d %(threadName)s) %(levelname)s - %(name)s: "%(message)s"'
)

console_output_handler = logging.StreamHandler(sys.stderr)
console_output_handler.setFormatter(formatter)
logger.addHandler(console_output_handler)
file_handler = logging.FileHandler("logs/api.log")
file_handler.setFormatter(formatter)
logger.setLevel(logging.ERROR)

from API import apihelper, types, util

"""
Module : OTRS
"""


class BackAPI:

    def __init__(self,  # token,
                 threaded=True, num_threads=4):
        """
        :param token: bot API token
        :return: Telebot object.
        """

        # self.token = token

        self.threaded = threaded
        if self.threaded:
            self.worker_pool = util.ThreadPool(num_threads=num_threads)

    def ticket_set_state(self, ticket_id: int, state: types.TicketState) -> None:
        return apihelper.ticket_set_state(ticket_id, state)

    def ticket_close(self, ticket_id: int) -> None:
        return apihelper.ticket_close(ticket_id)

    def ticket_create(self, title: str, user_email: str, body: str) -> int:
        """

        :param title:
        :param user_email:
        :param body:
        :return: returns id of created ticket
        """
        return apihelper.ticket_create(title, user_email, body)['ticketId']

    def ticket_get_by_user(self, user_email: str, page: Optional[int] = None, page_size: int = 5,
                           opened: Optional[bool] = None,
                           additional_fields: List[str] = None) -> List[types.Ticket]:
        """
        :param user_email:
        :param page:
        :param page_size:
        :param opened: None - return all ticket, True - opened, False - closed
        :param additional_fields:
        """
        return list(map(types.Ticket.de_json,
                        apihelper.ticket_get_by_user(user_email, page, page_size, opened, additional_fields)
                        ))

    def ticket_get_by_id(self, ticket_id: int, additional_fields: List[str] = None) -> types.Ticket:
        return types.Ticket.de_json(
            apihelper.ticket_get_by_id(ticket_id, additional_fields=additional_fields)
        )

    def ticket_send_message(self, ticket_id: int, message: str, from_user: str) -> None:
        return apihelper.ticket_send_message(ticket_id, message, from_user)
