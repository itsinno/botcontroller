from shared import db


class User(db.Model):
    email = db.Column(db.String(120), unique=True, nullable=False, primary_key=True)
    language = db.Column(db.String(2), nullable=False, default='EN')
    user_id = db.Column(db.String(120), nullable=False)
    messenger = db.Column(db.String(80), nullable=False)
    pending_messages = db.relationship('PendingMessage', backref='user', lazy=True, uselist=True)

    def __repr__(self):
        return '<User %r>' % self.email


class PendingMessage(db.Model):
    __tablename__ = 'PendingMessages'
    id = db.Column(db.Integer, primary_key=True)
    message = db.Column(db.String(4096), nullable=False)
    messenger = db.Column(db.String(80), nullable=False)
    user_email = db.Column(db.String(120), db.ForeignKey('user.email'), nullable=False)

